@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="card">
        <form action="/pertanyaan/update" method="POST">
            <div class="card-header">
                Form Edit Pertanyaan
            </div>
            <div class="card-body">
            @csrf
            @foreach($post as $p)
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" name="judul" id="judul" class="form-control" placeholder="Masukan Judul" value="{{$p->judul}}">
                </div>
                <div class="form-group">
                    <label for="isi">Isi Pertanyaan</label>
                    <textarea name="isi" id="isi" cols="30" rows="10" class="form-control">{{$p->isi}}</textarea>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
        @endforeach
    </div>
</div>

@endsection