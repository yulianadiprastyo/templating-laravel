@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <h1 class="mb-3">Hallo Selamat Datang <a href="/pertanyaan/create" class="btn btn-primary btn-sm">Tambah</a></h1>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Konten / isi</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($post as $key=>$value)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->isi}}</td>
                        <td>
                            <a href="" class="btn btn-primary btn-sm">Show</a>
                            <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <a href="/pertanyaan/delete/{{$value->id}}" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection