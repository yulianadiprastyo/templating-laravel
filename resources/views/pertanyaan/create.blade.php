@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="card">
            <form action="/pertanyaan/store" method="POST">
                @csrf
                <div class="card-header">
                    Form Create Pertanyaan
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" name="judul" id="judul" class="form-control" placeholder="Masukan Judul">
                    </div>
                    <div class="form-group">
                        <label for="isi">Isi Pertanyaan</label>
                        <textarea name="isi" id="isi" cols="30" rows="10" class="form-control"></textarea>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>

@endsection