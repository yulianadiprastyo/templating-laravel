<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>

<body>
    <div class="container">
        <section>
            <div class="title">
                <h1>Buat Account Baru!</h1>
            </div>
        <form action="/post" method="POST">
                <h4>Sign Up Form</h4>
                @csrf
                <!-- Fistname -->
                <div class="name">
                    <p>First name :</p>
                    <input type="text" name="first-name" id="first-name">
                </div>

                <!-- lastname -->
                <div class="last-name">
                    <p>Last name :</p>
                    <input type="text" name="last-name" id="last-name">
                </div>

                <!-- gender -->
                <div class="gender">
                    <p>Gender</p>
                    <!-- male -->
                    <div>
                        <input type="radio" name="gender" id="male" value="male">
                        <label for="male">Male</label>
                    </div>
                    <!-- Female -->
                    <div>
                        <input type="radio" name="gender" id="female" value="female">
                        <label for="female">Female</label>
                    </div>
                    <!-- Other -->
                    <div>
                        <input type="radio" name="gender" id="other" value="other">
                        <label for="other">Other</label>
                    </div>
                </div>

                <!-- Nationality -->
                <div class="nationality">
                    <p>Nationality</p>
                    <select name="nationality" id="nasional">
                        <option value="">--- Pilih ---</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Singapura">Singapura</option>
                    </select>
                </div>

                <!-- Language -->
                <div class="gender">
                    <p>Language Spoken</p>
                    <div>
                        <input type="radio" name="bahasa" id="Indonesia" value="Indonesia">
                        <label for="Indonesia">Bahasa Indonesia</label>
                    </div>
                    <div>
                        <input type="radio" name="bahasa" id="English" value="English">
                        <label for="English">English</label>
                    </div>
                    <div>
                        <input type="radio" name="bahasa" id="other" value="Other">
                        <label for="Other">Other</label>
                    </div>
                </div>

                <!-- Bio -->
                <div class="bio">
                    <p>Bio:</p>
                    <textarea name="Bio" id="" cols="30" rows="12"></textarea>
                </div>

                <!-- button signup -->
                <button type="submit">
                    Sign Up
                </button>
            </form>
        </section>
    </div>
</body>

</html>