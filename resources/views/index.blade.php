<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook</title>
</head>

<body>
    <div class="container">
        <section>
            <div class="title">
                <h1>SanberBook</h1>
            </div>
        </section>
        <section>
            <h4>Social Media Developer Santai Berkualias</h4>
            <p>Belajar dan Berbagi agar hidup ini semakian santai berkualitas</p>
        </section>
        <section>
            <h4>Benefit Join di SanberBook</h4>
            <ul>
                <li>Mendapatkan motivasi dari sesama developer</li>
                <li>Sharing khowledge dari para mastah Sanber</li>
                <li>Dibuta oleh calon web developer</li>
            </ul>
        </section>
        <section>
            <h4>Cara Bergabung ke SanberBook</h4>
            <ol>
                <li>Mengunjungi Website ini</li>
                <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
                <li>Selesai!</li>
            </ol>
        </section>
    </div>
</body>

</html>