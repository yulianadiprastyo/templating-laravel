<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@index');
Route::post('/post', 'AuthController@handleForm');
Route::get('/welcome', 'AuthController@welcome');

Route::get('/pertanyaan', 'PertanyaanCRUDController@index');
Route::get('/pertanyaan/create', 'PertanyaanCRUDController@create');
Route::post('/pertanyaan/store', 'PertanyaanCRUDController@store');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanCRUDController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanCRUDController@edit');
Route::post('/pertanyaan/{pertanyaan_id}', 'PertanyaanCRUDController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanCRUDController@destroy');

Route::group(['prefix' => 'admin'], function() 
{
    Route::get('/', 'AdminController@index');
    Route::get('/dataTable', 'AdminController@dataTable');
});
